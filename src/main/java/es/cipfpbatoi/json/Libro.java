package es.cipfpbatoi.json;

public class Libro {
    private int id;
    private String titulo;
    private String autor;
    private String editor;
    private String isbn;
    private short paginas;

    public Libro() {        
    }
    
    public Libro(int id, String titulo, String autor, String editor, String isbn, short paginas) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.editor = editor;
        this.isbn = isbn;
        this.paginas = paginas;
    }
    
   public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }
    
    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditor() {
        return editor;
    }
    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public short getPaginas() {
        return paginas;
    }
    public void setPaginas(short paginas) {
        this.paginas = paginas;
    }

    @Override
    public String toString() {
        return "Libro{" + "nombre=" + titulo + ", autor=" + autor + ", editor=" + editor + ", isbn=" + isbn + ", paginas=" + paginas + '}';
    }   

}
