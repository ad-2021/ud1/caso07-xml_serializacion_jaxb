package es.cipfpbatoi.json;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import es.cipfpbatoi.xml.*;

public class App {

	private static final String BIBLIOTECA_JSON = "biblio-gson.json";

	public static void main(String[] args) {

		ArrayList<Libro> listaLibros1 = new ArrayList<Libro>();

		// Creamos los libros
		Libro libro1 = new Libro();
		libro1.setId(1);
		libro1.setIsbn("978-0060554736");
		libro1.setTitulo("The Game");
		libro1.setAutor("Neil Strauss");
		libro1.setEditor("Harpercollins");
		libro1.setPaginas((short) 200);
		listaLibros1.add(libro1);

		Libro libro2 = new Libro();
		libro2.setId(2);
		libro2.setIsbn("978-3832180577");
		libro2.setTitulo("Feuchtgebiete");
		libro2.setAutor("Charlotte Roche");
		libro2.setEditor("Dumont Buchverlag");
		libro2.setPaginas((short) 100);
		listaLibros1.add(libro2);

		Libro libro3 = new Libro(3, "Otro libro", "Autor X", "Editor X", "123-412343423", (short) 100);
		listaLibros1.add(libro3);

		// Creamos la biblioteca y le asignamos los libros
		Biblioteca biblioteca1 = new Biblioteca();
		biblioteca1.setNombre("Fraport Bookstore");
		biblioteca1.setUbicacion("Frankfurt Airport");
		biblioteca1.setListaLibros(listaLibros1);

		// Serializamos
		Gson gson;
		gson = new Gson();
		// Si queremos formato pretty:
//		gson = new GsonBuilder().setPrettyPrinting().create();
		try (BufferedWriter bw = Files.newBufferedWriter(Paths.get((BIBLIOTECA_JSON)))) {
			String resultadoJson = gson.toJson(biblioteca1);
			bw.write(resultadoJson);
			System.out.println(resultadoJson);
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

		System.out.println("....");
		// Deserializamos
		try {
			List<String> lineas = Files.readAllLines(Paths.get(BIBLIOTECA_JSON));
			String contenidoJson = "";
			for (String lin : lineas) {
				contenidoJson = contenidoJson + lin;
			}
//			gson = new GsonBuilder().create();
			Biblioteca biblioteca2 = gson.fromJson(contenidoJson, Biblioteca.class);
			
			if (biblioteca2 != null) {
                List<Libro> listaLibros2 = biblioteca2.getListaLibros();
                System.out.println("Nombre: " + biblioteca2.getNombre());
                System.out.println("Ubicacion: " + biblioteca2.getUbicacion());
//                for (Libro libro : listaLibros2) {
//                    System.out.println(libro);
//                }
                listaLibros2.stream().forEach(System.out::println);
            }
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
}
